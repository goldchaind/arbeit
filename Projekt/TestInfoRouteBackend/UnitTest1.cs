
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InfoRouteBackend.InfoGraph;
using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace TestInfoRouteBackend
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            using var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            var logger = loggerFactory.CreateLogger<InfoGraph>();



            InfoGraph graph = new InfoGraph(logger);

            Vertex[] vertices = graph.Vertices;

            Stopwatch normalTime = new Stopwatch();
            Stopwatch cachedTime = new Stopwatch();

            foreach (Vertex v in vertices)
            {

                normalTime.Start();
                List<Edge> normalEdges = graph.GetEdgesForVertex(v.ID);
                normalTime.Stop();
                cachedTime.Start();
                List<Edge> cachedEdges = graph.GetCachedEdgesForVertex(v.ID);
                cachedTime.Stop();
                Assert.AreEqual(normalEdges.Count, cachedEdges.Count, 0, "Ungleiche Elementanzahl");


            }

            logger.LogInformation("Normaltime: {0} seconds, Cachedtime: {1} seconds", normalTime.Elapsed.TotalSeconds, cachedTime.Elapsed.TotalSeconds);


        }

        private bool ListEquals(List<Edge> l1, List<Edge> l2)
        {
            if (l1.Count != l2.Count)
            {
                return false;
            }

            foreach(Edge e in l1)
            {
                if(l2.Contains(e) == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
