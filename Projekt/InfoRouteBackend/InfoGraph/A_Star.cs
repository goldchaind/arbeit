using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace InfoRouteBackend.InfoGraph;

public class A_Star<T> : PathFinder<T> where T : Vertex
{
    private class Node : IComparable<Node>
    {
        public T Vertex;
        public Node Predessor;
        public double RealCost;
        public double HeuristicCostToDestination;
        public double CombinedCost { get { return RealCost + HeuristicCostToDestination; } }

        public Node(T vertex)
        {
            Vertex = vertex;
            Predessor = null;
            RealCost = double.MaxValue;
            HeuristicCostToDestination = 0;
        }

        public int CompareTo(Node other)
        {
            return CombinedCost.CompareTo(other.CombinedCost);
        }
    }

    private Heurisitc<T> Heuristic;
    private RouteCache<T> RouteCache;
    private AnnotationManager AnnotationManager;
    public A_Star(IGraph<T> graph, Heurisitc<T> heuristicFunction, T start, T end, AnnotationManager annotationManager = null) : base(graph, start, end)
    {
        RouteCache = graph.RouteCache;
        Heuristic = heuristicFunction;
        AnnotationManager = annotationManager;
        calculationStartTime = TimeSpan.Zero;
    }

    private Dictionary<int, Node> OpenList = new Dictionary<int, Node>();
    private Dictionary<int, Node> ClosedList = new Dictionary<int, Node>();
    private Node FindNodeForVertex(T vertex)
    {
        Node node = null;
        if (OpenList.TryGetValue(vertex.ID, out node))
        {
            return node;
        }
        else
        {
            ClosedList.TryGetValue(vertex.ID, out node);
        }
        return node;
    }


    private Node GetNextActiveNode()
    {
        double currentMinCost = double.PositiveInfinity;
        Node currentMinNode = null;
        foreach (Node node in OpenList.Values)
        {
            if (node.CombinedCost < currentMinCost)
            {
                currentMinCost = node.CombinedCost;
                currentMinNode = node;
            }
        }

        OpenList.Remove(currentMinNode.Vertex.ID);
        ClosedList.Add(currentMinNode.Vertex.ID, currentMinNode);
        return currentMinNode;
    }

    public TimeSpan calculationStartTime { get; set; }
    public override void CalculatePath()
    {
        if (calculationStartTime == TimeSpan.Zero)
        {
            calculationStartTime = DateTime.Now.TimeOfDay;
        }
        if (RouteCache != null)
        {
            List<T> cacheList = RouteCache.GetFromCache(Start, End);

            if (cacheList != null)
            {
                path = cacheList;
                IsCalculated = true;
                IsCalculating = false;
                PathFound = true;
                return;
            }
        }

        IsCalculating = true;
        IsCalculated = false;
        PathFound = false;
        Node StartNode = new Node(Start);
        StartNode.RealCost = 0;
        StartNode.HeuristicCostToDestination = 0;

        OpenList.Add(StartNode.Vertex.ID, StartNode);

        while (OpenList.Count > 0)
        {
            Node currentNode = GetNextActiveNode();

            if (currentNode.Vertex == End)
            {
                PathFound = true;
                break;
            }
            else
            {
                ExpandNode(currentNode);
            }
        }
        if (PathFound)
        {
            BuildPath();
        }
        IsCalculating = false;
        IsCalculated = true;
    }
    private void BuildPath()
    {
        Node currentNode = FindNodeForVertex(End);
        while (currentNode.Vertex != Start)
        {
            path.Add(currentNode.Vertex);
            currentNode = currentNode.Predessor;
        }
        path.Add(Start);
        path.Reverse();
        if (RouteCache != null)
        {
            RouteCache.AddPath(Start, End, path);
        }
    }

    private void ExpandNode(Node currentNode)
    {
        List<Edge<T>> successors = Graph.GetEdgesForVertex(currentNode.Vertex);

        foreach (Edge<T> edge in successors)
        {
            Annotation vertexAnnotation = edge.To.Annotation;
            if(vertexAnnotation != null && AnnotationManager!= null && 
                !AnnotationManager.AnnotationActiveInHirarchy(vertexAnnotation))
            {
                continue;
            }

            Node currentSuccessor = FindNodeForVertex(edge.To);
            if (currentSuccessor == null)
            {
                currentSuccessor = new Node(edge.To);
            }

            if (ClosedList.ContainsKey(currentSuccessor.Vertex.ID) == false)
            {
                double newRealCost = currentNode.RealCost + edge.Cost;
                if(vertexAnnotation != null)
                {
                    if(vertexAnnotation.GetType() == typeof(TimeAnnotation))
                    {
                        TimeAnnotation timeAnnotation = 
                            (TimeAnnotation)vertexAnnotation;

                        TimeSpan timeToCurrentNode = 
                            new TimeSpan(0, 0, 0, (int) (currentNode.RealCost / 1.7));

                        TimeSpan timeToAdd = 
                            timeAnnotation.CalcTimeToNextAvailable
                            (calculationStartTime.Add(timeToCurrentNode));

                        newRealCost += timeToAdd.TotalSeconds * 1.7;

                    }else if(vertexAnnotation.GetType() == typeof(DurationAnnotation))
                    {
                        DurationAnnotation durationAnnotation = 
                            (DurationAnnotation)vertexAnnotation;
                        newRealCost += durationAnnotation.Duration.TotalSeconds * 1.7;
                    }
                }
                if (newRealCost < currentSuccessor.RealCost)
                {
                    currentSuccessor.Predessor = currentNode;
                    currentSuccessor.RealCost = newRealCost;
                    currentSuccessor.HeuristicCostToDestination
                        = Heuristic.CalculateHeuristic(currentSuccessor.Vertex, End);


                    if (OpenList.ContainsKey(currentSuccessor.Vertex.ID) == false)
                    {
                        OpenList.Add(currentSuccessor.Vertex.ID, currentSuccessor);
                    }
                }
            }
        }
    }
}

