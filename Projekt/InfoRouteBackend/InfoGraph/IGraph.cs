﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoRouteBackend.InfoGraph
{
    public interface IGraph<T>  where T : Vertex
    {       
        public int VerticesCount { get; }
        public int EdgesCount { get; }        
        public List<T> Vertices { get; }
        public RouteCache<T> RouteCache { get; }
        public T GetVertexById(int id);
        public List<Edge<T>> GetEdgesForVertex(T v);
    }
}
