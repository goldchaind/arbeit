﻿using System;

namespace InfoRouteBackend.InfoGraph
{
    public interface Heurisitc<T> where T : Vertex
    {
        public double CalculateHeuristic(T vertex1, T vertex2);
    }
    public class InfoEuklidHeuristic : Heurisitc<InfoVertex>
    {
        public double CalculateHeuristic(InfoVertex vertex1, InfoVertex vertex2)
        {
            double deltaX = vertex1.X - vertex2.X;
            double deltaY = vertex1.Y - vertex2.Y;
            return Math.Sqrt(Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2));
        }
    }
    public class InfoManhattenHeuristic : Heurisitc<InfoVertex>
    {        public double CalculateHeuristic(InfoVertex vertex1, InfoVertex vertex2)
        {
            double deltaX = vertex1.X - vertex2.X;
            double deltaY = vertex1.Y - vertex2.Y;
            return Math.Abs(deltaX) + Math.Abs(deltaY);
        }
    }
}
