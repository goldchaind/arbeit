﻿using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph
{
    public class EdgeList<T> : Dictionary<int, List<Edge<T>>> where T : Vertex
    {        
        public void AddEdge(Edge<T> edge)
        {
            List<Edge<T>> edgesForVertex;
            if (base.ContainsKey(edge.From.ID))
            {
                edgesForVertex = base[edge.From.ID];
            }
            else
            {
                edgesForVertex = new List<Edge<T>>();
                Add(edge.From.ID, edgesForVertex);
            }
            edgesForVertex.Add(edge);
        }
    }
}

