﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoRouteBackend.InfoGraph
{
    public class Vertex
    {
        public Annotation Annotation { get; set; }
        public Vertex(int id)
        {
            ID = id;            
        }
        public int ID { get; set; }       
    }

    public class InfoVertex : Vertex
    {
        public InfoVertex(int id, double x, double y, int level) : base(id)
        {
            X = x;
            Y = y;
            Level = level;
        }
        public double X { get; private set; }
        public double Y { get; private set; }
        public int Level { get; private set; }

        public override string ToString()
        {
            return "ID: " + ID + "{" + X + "|" + Y + "|" + Level + "}";
        }
    }
}
