﻿using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph
{
    public class RouteCache<T> : Dictionary<(T, T), List<T>> where T : Vertex
    {        
        public void AddPath(T start, T end, List<T> route)
        {
            Add((start, end), route);    
        }
        public List<T> GetFromCache(T start, T end){
            List<T> list = new List<T>();
            if(TryGetValue((start,end),out list))
            {
                return list;
            }
            if(TryGetValue((end,start),out list)){
                list.Reverse();
                return list;
            }
            return null;
        }
    }
}
