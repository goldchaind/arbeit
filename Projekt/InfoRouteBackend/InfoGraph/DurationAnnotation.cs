﻿using System;

namespace InfoRouteBackend.InfoGraph
{
    public class DurationAnnotation : Annotation
    {

        public DurationAnnotation(string name) : base(name)
        {
        }

        public DurationAnnotation(string name,TimeSpan duration) : base(name)
        {
            Duration = duration;
        }

        public TimeSpan Duration { get; set; }


    }
}
