﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoRouteBackend.InfoGraph
{
    public class Edge<T> where T:Vertex
    {
        public Edge(int id, T from, T to,double cost)
        {
            ID = id;
            From = from;
            To = to;
            Cost = cost;
        }        
        public int ID { get; private set; }
        public T From { get; private set; }
        public T To { get; private set; }
        public double Cost { get; private set; }
    }
}
