﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace InfoRouteBackend.InfoGraph
{

    public class InfoGraph : IGraph<InfoVertex>
    {
        private readonly List<InfoVertex> vertices;
        private readonly Dictionary<int, InfoVertex> verticesDict;
        private readonly List<Map> maps;
        private readonly EdgeList<InfoVertex> edges;
        private readonly RouteCache<InfoVertex> routeCache;
        private readonly List<int> levels;        
        private readonly ILogger<InfoGraph> logger;

        public Map[] Maps => maps.ToArray();
        public List<InfoVertex> Vertices => new List<InfoVertex>(vertices);
        public int VerticesCount => vertices.Count;
        public int EdgesCount => edges.Count;
        public int[] Levels => levels.ToArray();

        public RouteCache<InfoVertex> RouteCache => routeCache;

        public InfoGraph(ILogger<InfoGraph> logger)
        {
            this.logger = logger;
            vertices = new List<InfoVertex>();
            verticesDict = new Dictionary<int, InfoVertex>(100000);
            edges = new EdgeList<InfoVertex>();
            routeCache = new RouteCache<InfoVertex>();
            maps = new List<Map>();
            levels = new List<int>();
            LoadVerteciesAndEdges();
            LoadMaps();
        }
        private void LoadVerteciesAndEdges()
        {
            Stopwatch watch = Stopwatch.StartNew();
            string GraphPath = "RoutingData/routing_muc_20190730_graph.prt";

            XElement XMLGraph = XElement.Load(GraphPath);
            IEnumerable<XElement> XMLvertices = XMLGraph.Elements("vertex");
            foreach (XElement XMLvertex in XMLvertices)
            {
                int id = int.Parse(XMLvertex.Attribute("id").Value);
                double x = (double)XMLvertex.Attribute("x");
                double y = (double)XMLvertex.Attribute("y");
                int z = (int)XMLvertex.Attribute("z");
                InfoVertex vertex = new InfoVertex(id, x, y, z);
                verticesDict.Add(id, vertex);
                vertices.Add(vertex);
            }
            logger.LogInformation("Loaded {0} Vertices in {1} seconds", vertices.Count, watch.Elapsed.TotalSeconds);
            watch.Restart();
            IEnumerable<XElement> XMLedges = XMLGraph.Elements("edge");
            foreach (XElement XMLedge in XMLedges)
            {
                int id = int.Parse(XMLedge.Attribute("id").Value);
                int fromId = int.Parse(XMLedge.Attribute("from").Value);
                int toId = int.Parse(XMLedge.Attribute("to").Value);
                double distance = (double)XMLedge.Attribute("dist");

                InfoVertex from = GetVertexById(fromId);
                InfoVertex to = GetVertexById(toId);
                if (from != null && to != null)
                {
                    Edge<InfoVertex> edge = new Edge<InfoVertex>(id, from, to, distance);                   
                    edges.AddEdge(edge);
                }
            }
            vertices.Sort((x, y) => x.Level.CompareTo(y.Level));        

            logger.LogInformation("Loaded {0} Edges in {1} seconds", edges.Count, watch.Elapsed.TotalSeconds);
        }
        private void LoadMaps()
        {
            Stopwatch watch = Stopwatch.StartNew();
            string GraphPath = "RoutingData/routing_muc_20190730_maps.prt";

            XElement XMLGraph = XElement.Load(GraphPath);
            IEnumerable<XElement> XMLmaps = XMLGraph.Elements("map");
            foreach (XElement XMLmap in XMLmaps)
            {
                int level = int.Parse(XMLmap.Attribute("level").Value);
                if(levels.Contains(level) == false)
                {
                    levels.Add(level);
                }
                double minX = (double)(XMLmap.Attribute("minX"));
                double maxX = (double)(XMLmap.Attribute("maxX"));
                double minY = (double)(XMLmap.Attribute("minY"));
                double maxY = (double)(XMLmap.Attribute("maxY"));
                string bitmappath = "RoutingData/" + XMLmap.Attribute("bitmap").Value;
                Map map = new Map(level, minX, maxX, minY, maxY, bitmappath);
                maps.Add(map);
            }
            logger.LogInformation("Loaded {0} Vertices in {1} seconds", vertices.Count, watch.Elapsed.TotalSeconds);
            watch.Stop();
        }

        public List<Edge<InfoVertex>> GetEdgesForVertex(InfoVertex v)
        {
            return GetEdgesForVertex(v.ID);
        }
        public List<Edge<InfoVertex>> GetEdgesForVertex(int id)
        {
            List<Edge<InfoVertex>> edgesForVertex;
            if(edges.TryGetValue(id, out edgesForVertex)){
                return edgesForVertex;
            }
            else
            {
                return new List<Edge<InfoVertex>>();
            }
        }
        public List<InfoVertex> GetVerticesRange(int start, int count)
        {
            if (start > VerticesCount)
            {
                start = VerticesCount;
            }
            if (start + count >= VerticesCount)
            {
                count = VerticesCount - start;
            }
            return vertices.GetRange(start, count);
        }
        public int GetVerticesCountForLevel(int level)
        {
            return vertices.FindAll(vertex => vertex.Level == level).Count();
        }

        public int GetVerticesStartByLevel(int level)
        {
            return vertices.FindIndex(vertex => vertex.Level == level);
        }
        public InfoVertex GetVertexById(int id)
        {
            InfoVertex vertex = null;
            verticesDict.TryGetValue(id, out vertex);
            return vertex;
        }
    }
}
