﻿using System;
using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph
{
    public class Annotation : Vertex
    {
        public string Name { get; private set; }
        public List<Annotation> Parents { get; internal set; }
        public List<Annotation> Children { get; internal set; }

        public Annotation(string name) : base(0)
        {
            Active = true;
            Name = name;
            Parents = new List<Annotation>();
            Children = new List<Annotation>();
            base.Annotation = null;
        }
        public bool Active { get; set; }

        public void AddParents(params Annotation[] parents)
        {
            foreach (Annotation parent in parents)
            {
                Parents.Add(parent);
                parent.Children.Add(this);
            }
        }

        public void AddChildren(params Annotation[] childrens)
        {
            foreach(Annotation child in childrens)
            {
                child.AddParents(this);
            }
        }
    }
}