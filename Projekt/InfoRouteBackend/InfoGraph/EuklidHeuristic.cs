﻿using System;

namespace InfoRouteBackend.InfoGraph
{
    public class InfoEuklidHeuristic1 : Heurisitc<InfoVertex>
    {
        public double CalculateHeuristic(InfoVertex vertex1, InfoVertex vertex2)
        {
            double deltaX = vertex1.X - vertex2.X;
            double deltaY = vertex1.Y - vertex2.Y;
            return Math.Sqrt(Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2));
        }
    }
}
