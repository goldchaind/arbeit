using System;
using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph;

public abstract class PathFinder<T> where T : Vertex
{
    public T Start { get; }
    public T End { get; }

    protected IGraph<T> Graph;

    protected List<T> path = new List<T>();

    protected PathFinder(IGraph<T> graph, T start, T end)
    {
        Graph = graph;
        Start = start;
        End = end;
    }
    public T[] Path
    {
        get
        {
            if (IsCalculated == false)
            {
                throw new Exception("Path is not calculated yet");
            }
            return path.ToArray();
        }
    }
    public bool IsCalculated { get; protected set; } = false;
    public bool IsCalculating { get; protected set; } = false;
    public bool PathFound { get; protected set; } = false;
    public abstract void CalculatePath();

}