﻿using System;
using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph
{
    public class AnnotationManager : IGraph<Annotation>
    {
        List<Annotation> rootAnnotations = new List<Annotation>();

        public int VerticesCount => throw new System.NotImplementedException();

        public int EdgesCount => throw new System.NotImplementedException();

        public List<Annotation> Vertices { get; set; }

        private Dictionary<Annotation, List<Edge<Annotation>>> edgeCache = new Dictionary<Annotation, List<Edge<Annotation>>>();

        public RouteCache<Annotation> RouteCache { get; set; }

        public AnnotationManager()
        {
            Vertices = new List<Annotation>();
            RouteCache = new RouteCache<Annotation>();
        }

        public T AddAvailableAnnotation<T>(T annotation) where T : Annotation
        {
            annotation.ID = Vertices.Count;
            Vertices.Add(annotation);
            return annotation;
        }

        public void CalculateAnnotationHirarchy()
        {
            rootAnnotations = Vertices.FindAll(annotation => annotation.Active && annotation.Parents.Count == 0);
            foreach (Annotation annotation in Vertices)
            {
                List<Edge<Annotation>> edgeList = new List<Edge<Annotation>>();
                foreach (Annotation child in annotation.Children)
                {
                    Edge<Annotation> e = new Edge<Annotation>(0, annotation, child, 1);
                    edgeList.Add(e);
                }
                edgeCache.Add(annotation, edgeList);
            }
        }

        public bool AnnotationActiveInHirarchy(Annotation annotation)
        {
            foreach (Annotation rootAnnotation in rootAnnotations)
            {
                if (rootAnnotation.Active)
                {
                    A_Star<Annotation> pathfinder = new A_Star<Annotation>(this, new AnnotationHeuristic(), rootAnnotation, annotation);
                    pathfinder.CalculatePath();
                    if (pathfinder.PathFound)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<Edge<Annotation>> GetEdgesForVertex(Annotation v)
        {
            return edgeCache[v].FindAll(edge=>edge.From.Active);
        }

        public Annotation GetVertexById(int id)
        {
            throw new System.NotImplementedException();
        }

        public class AnnotationHeuristic : Heurisitc<Annotation>
        {
            public double CalculateHeuristic(Annotation vertex1, Annotation vertex2)
            {
                return 0;
            }
        }
    }
}