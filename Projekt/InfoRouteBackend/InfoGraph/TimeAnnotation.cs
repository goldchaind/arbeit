﻿using System;
using System.Collections.Generic;

namespace InfoRouteBackend.InfoGraph
{
    public class TimeAnnotation : Annotation
    {
        public TimeAnnotation(string name) : base(name)
        {
            ActiveTimes = new Dictionary<TimeSpan, TimeSpan>();
        }
        public Dictionary<TimeSpan, TimeSpan> ActiveTimes { get; set; }

        public TimeSpan CalcTimeToNextAvailable(TimeSpan time)
        {
            TimeSpan currentMinTimeSpan = TimeSpan.MaxValue;
            foreach (TimeSpan activeTime in ActiveTimes.Keys)
            {
                TimeSpan waitTime = activeTime.Subtract(time);

                if (waitTime.Add(ActiveTimes[activeTime]).Ticks > 0)
                {
                    return TimeSpan.Zero;
                }
                else if (waitTime.Ticks > 0 && waitTime.CompareTo(currentMinTimeSpan) < 0)
                {
                    currentMinTimeSpan = waitTime;
                }
            }
            return currentMinTimeSpan;
        }
    }
}
