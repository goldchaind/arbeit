using System.IO;

namespace InfoRouteBackend.InfoGraph
{
    public class Map
    {
        public int Level { get; private set; }
        public double MinX { get; private set; }
        public double MaxX { get; private set; }
        public double MinY { get; private set; }
        public double MaxY { get; private set; }
        private byte[] BitmapData { get;  set; }

        public Map(int level,double minX, double maxX, double minY, double maxY, string bitmapPath)
        {
            Level = level;
            MinX = minX;
            MaxX = maxX;
            MinY = minY;
            MaxY = maxY;
            BitmapData = File.ReadAllBytes(bitmapPath);
        }

        public byte[] GetBitmapData()
        {
            return BitmapData;
        }
    }
}