﻿using InfoRouteBackend.InfoGraph;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace InfoRouteBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InfoGraphController : ControllerBase
    {
        private readonly ILogger<InfoGraphController> logger;
        private readonly InfoGraph.InfoGraph graph;

        public InfoGraphController(ILogger<InfoGraphController> logger, IGraph<InfoVertex> graph)
        {
            this.logger = logger;
            this.graph = (InfoGraph.InfoGraph)graph;
        }

        [HttpGet("Levels")]
        public int[] GetLevels()
        {
            return graph.Levels;
        }

        [HttpGet("Maps")]
        public Map[] GetMaps()
        {
            return graph.Maps;
        }


        [HttpGet("Maps/{level}")]
        public Map GetMap(int? level)
        {
            return graph.Maps.First(m => m.Level == level);
        }

        [HttpGet("Maps/Bitmap/{level}")]
        public IActionResult GetBitmap(int? level)
        {
            Map m = GetMap(level);
            return File(m.GetBitmapData(), "image/bmp");
        }

        [HttpGet("Vertecies/Count")]
        public int GetVerteciesCount(int level)
        {
            return graph.GetVerticesCountForLevel(level);
        }

        [HttpGet("Vertecies/Start")]
        public int GetVerteciesStart(int level)
        {
            return graph.GetVerticesStartByLevel(level);
        }

        [HttpGet("Edges/Count")]
        public int GetEdgesCount()
        {
            return graph.EdgesCount;
        }

        [HttpGet("Edges")]
        public IEnumerable<Edge<InfoVertex>> GetEdgesForVertex(int vertexID)
        {
            return graph.GetEdgesForVertex(vertexID);
        }

        [HttpGet("Vertecies")]
        public IEnumerable<Vertex> GetVertecies(int start, int count)
        {
            return graph.GetVerticesRange(start, count);
        }

        [HttpGet("Path")]
        public Vertex[] GetPath(int startID, int endID)
        {
            A_Star<InfoVertex> path = new A_Star<InfoVertex>(graph, new InfoEuklidHeuristic(), graph.GetVertexById(startID), graph.GetVertexById(endID));
            path.CalculatePath();
            List<int> levels = new List<int>();
            string l = "{";
            foreach (InfoVertex v in path.Path)
            {
                if (levels.Contains(v.Level) == false)
                {
                    levels.Add(v.Level);
                    l += v.Level.ToString() + ",";
                }
            }

            l += "}";
            logger.LogInformation($"Pathfound: {path.PathFound} on Levels:" + l);
            return path.Path;
        }

        [HttpGet("Annotations")]
        public bool getAnnotationTest()
        {
            /*
             *  0 0 0 0 0 0 0 0 0 
                1 0 0 0 0 0 0 0 0 
                1 0 0 0 0 0 0 0 0 
                0 0 1 0 0 0 0 0 0 
                0 0 0 1 0 0 0 0 0 
                0 0 1 0 0 0 0 0 0 
                0 0 0 0 0 0 1 0 0 
                0 0 0 0 0 1 0 0 0 
                0 0 0 0 1 0 1 0 0 */

            AnnotationManager annotationManager = new AnnotationManager();

            Annotation terminal2 = annotationManager.AddAvailableAnnotation(new Annotation("Terminal 2"));

            Annotation publicArea = annotationManager.AddAvailableAnnotation(new Annotation("Öffentlicher Bereich"));

            Annotation securityCheck = annotationManager.AddAvailableAnnotation(new Annotation("Sicherheitskontrolle"));

            CheckAnnotation staffID = annotationManager.AddAvailableAnnotation(new CheckAnnotation("FMG Ausweiß"));
            DurationAnnotation staffSecurityCheck = annotationManager.AddAvailableAnnotation(new DurationAnnotation("Personal Kontrolle"));

            CheckAnnotation airplaneTicket = annotationManager.AddAvailableAnnotation(new CheckAnnotation("Flugticket"));
            DurationAnnotation passengerSecurityCheck = annotationManager.AddAvailableAnnotation(new DurationAnnotation("Passagier Kontrolle"));
            TimeAnnotation passengerSecurityCheckTimes = annotationManager.AddAvailableAnnotation(new TimeAnnotation("Passagier Kontrolle Öffnungszeiten"));

            Annotation nonPublicArea = annotationManager.AddAvailableAnnotation(new Annotation("Sicherheitsbereich Terminal 2"));

            Annotation DutyFree = annotationManager.AddAvailableAnnotation(new Annotation("DutyFree Shop"));

            terminal2.AddChildren(publicArea, securityCheck);
            securityCheck.AddChildren(staffID, airplaneTicket);

            staffID.AddChildren(staffSecurityCheck);

            airplaneTicket.AddChildren(passengerSecurityCheckTimes);
            passengerSecurityCheckTimes.AddChildren(passengerSecurityCheck);

            nonPublicArea.AddParents(passengerSecurityCheck, staffSecurityCheck);
            nonPublicArea.AddChildren(DutyFree);

            annotationManager.CalculateAnnotationHirarchy();
          
            bool test = annotationManager.AnnotationActiveInHirarchy(DutyFree);
           


            return test;
        }
    }
}
