﻿using InfoRouteBackend.InfoGraph;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        static InfoGraph graph;
        static Vertex[] vertices;
        static ILogger<InfoGraph> logger;
        static void Main(string[] args)
        {
            using var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            logger = loggerFactory.CreateLogger<InfoGraph>();
            graph = new InfoGraph(logger);

            vertices = graph.Vertices;

            Stopwatch normalTime = new Stopwatch();
            Stopwatch cachedTime = new Stopwatch();

            foreach (Vertex v in vertices)
            {

                normalTime.Start();
                List<Edge> normalEdges = graph.GetEdgesForVertex(v.ID);
                normalTime.Stop();
                cachedTime.Start();
                List<Edge> cachedEdges = graph.GetCachedEdgesForVertex(v.ID);
                cachedTime.Stop();
                if (ListEquals(normalEdges, cachedEdges) == false){
                    logger.LogError("Non matching Lists for ID:{0}", v.ID);
                }
                if (v.ID % 100 == 0)
                {
                    logger.LogInformation("CurrID:{0} Normaltime: {1} seconds, Cachedtime: {2} seconds", v.ID, normalTime.Elapsed.TotalSeconds, cachedTime.Elapsed.TotalSeconds);
                }
            }
            logger.LogInformation("Normaltime: {0} seconds, Cachedtime: {1} seconds", normalTime.Elapsed.TotalSeconds, cachedTime.Elapsed.TotalSeconds);
            logger.LogInformation("Done");
            Console.ReadLine();
        }
       

        private static bool ListEquals(List<Edge> l1, List<Edge> l2)
        {
            if (l1.Count != l2.Count)
            {
                return false;
            }

            foreach (Edge e in l1)
            {
                if (l2.Contains(e) == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
