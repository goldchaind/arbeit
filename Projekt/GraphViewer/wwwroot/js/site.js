﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var backendBaseURL = 'https://localhost:44357/InfoGraph/';

var canvasWidth = 1280;
var canvasHeight = 800;
var canvas = document.getElementById("canvas");
var device = canvas.getContext("2d");

//Zoom
const zoomIntensity = 0.02;
let scale = 1;
let originx = 0;
let originy = 0;
let visibleWidth = canvasWidth;
let visibleHeight = canvasHeight;

//Logic
var levels;
var currentLevelIndex = 0;
var bitmapCache;

let maps;

let vertices;

let activeVertex;
let activeEdges;

let secondVertex;

let path;

var debugLabel = document.getElementById("debugLabel");

//First import of the levels and creation of the bitmap Cache
fetch(backendBaseURL + "Levels")
    .then(response => response.json())
    .then(data => {
        levels = data;
        bitmapCache = Array(levels.length);
        vertices = Array(levels.length);

        for (let i = 0; i < levels.length; i++) {
            GetBitmapFromMapLevel(i, levels[i]);
        }
        fetch(backendBaseURL + "Maps")
            .then(response => response.json())
            .then(data => {
                maps = data;
                loadVertices();
            });

    });

canvas.addEventListener("wheel", event => {
    const direction = Math.sign(event.deltaY);
    if (event.shiftKey === true) {
        changeCurrentLevel(direction);
        console.log(currentLevelIndex + " " + levels[currentLevelIndex]);
    } else {
        scale -= scale * 0.05 * direction;
        scale = Math.round(scale * 1000) / 1000;
        device.clearRect(0, 0, canvasWidth, canvasHeight);
        zoomRect.x = canvasWidth * (scale - 1) * -(translatedMouseX / canvasWidth);
        zoomRect.y = canvasHeight * (scale - 1) * -(translatedMouseY / canvasHeight);
        zoomRect.w = canvasWidth * scale;
        zoomRect.h = canvasHeight * scale;
        draw();
        updateDebugText();
    }
});

const direction = {
    left: 1,
    right: 2,
    up: 3,
    down: 4
}


function GetPath() {
    fetch(backendBaseURL + `Path?startID=${activeVertex.id}&endID=${secondVertex.id}`)
        .then(response => response.json())
        .then(data => {
            path = data;
            draw();
        });
}


let mousedownTracker = -1;
canvas.addEventListener("mousedown", event => {
    if (event.button === 2)//Right mouse button
    {
        if (mousedownTracker == -1) {
            mousedownTracker = setInterval(scroll, 50 /*execute every 100ms*/);
        }
    }
    if (event.button === 0) {
        if (event.shiftKey === true) {
            if (activeVertex == null) {
                activeVertex = getVertexMouseCollision();
            } else {
                secondVertex = getVertexMouseCollision();
                GetPath();

            }
        } else {
            secondVertex = null;
            path = null;
            activeVertex = getVertexMouseCollision();
            console.log(activeVertex);
            getActiveEdgesForVertex(activeVertex);
            draw();
        }
    }
});

canvas.addEventListener("mouseup", event => {

    if (event.button === 2) {
        if (mousedownTracker !== -1) {
            console.log("Mouseup");
            clearInterval(mousedownTracker);
            mousedownTracker = -1;
        }
    }
});

function scroll() {
    let scrollFactor = 0.01;

    if (mouseX < canvasWidth * 0.2) {
        zoomRect.x += canvasWidth * scrollFactor;
    } else if (mouseX > canvasWidth - canvasWidth * 0.2) {
        zoomRect.x -= canvasWidth * scrollFactor;
    }
    if (mouseY < canvasHeight * 0.2) {
        zoomRect.y += canvasHeight * scrollFactor;
    } else if (mouseY > canvasHeight - canvasHeight * 0.2) {
        zoomRect.y -= canvasHeight * scrollFactor;
    }
    draw();
}

function draw() {
    device.clearRect(0, 0, canvasWidth, canvasHeight);
    device.drawImage(bitmapCache[currentLevelIndex], zoomRect.x, zoomRect.y, zoomRect.w, zoomRect.h);


   
    
    if (scale > 5) {
        drawVertices();
    }

    if (path != null) {
        path.forEach(v => {
            //drawEdges(v);
            drawVertex(v,'yellow');

        });
    }
    
    if (activeVertex != null) {
        drawVertex(activeVertex, 'blue');
        if (activeEdges != null) {
            if (path == null) {
                activeEdges.forEach(edge => {
                    drawVertex(edge.to, 'yellow');
                });
            }
        }
    }

    

    if (secondVertex != null) {
        drawVertex(secondVertex, 'red');
    }
    updateDebugText();
}

function drawEdges(v) {
    fetch(backendBaseURL + "Edges?vertexID=" + v.id)
        .then(result => result.json())
        .then(edges => {

            edges.forEach(edge => {
                let xRange = maps[currentLevelIndex].maxX - maps[currentLevelIndex].minX;
                let yRange = maps[currentLevelIndex].maxY - maps[currentLevelIndex].minY;

                let x1 = (edge.from.x - maps[currentLevelIndex].minX) / xRange * zoomRect.w + zoomRect.x;
                let y1 = (edge.from.y - maps[currentLevelIndex].minY) / yRange * zoomRect.h + zoomRect.y;
                
                let x2 = (edge.to.x - maps[currentLevelIndex].minX) / xRange * zoomRect.w + zoomRect.x;
                let y2 = (edge.to.y - maps[currentLevelIndex].minY) / yRange * zoomRect.h + zoomRect.y;
                if(edge.distance === 1.7){
                    drawLine(x1,y1,x2,y2,2);
                }else{
                    drawLine(x1,y1,x2,y2,4,'green');
                }
                drawVertex(v, 'yellow');
            });


        });
}

function drawLine(x1,y1,x2,y2,size,color='blue'){
    device.strokeStyle = color;
    device.lineWidth = size;

    // draw a red line
    device.beginPath();
    device.moveTo(x1, y1);
    device.lineTo(x2, y2);
    device.stroke();
}

class Rect {
    constructor(x, y, w, h) {
        [this.x, this.y, this.w, this.h] = [x, y, w, h];
    }
}

let mouseX = 0;
let mouseY = 0;
let translatedMouseX;
let translatedMouseY;
let zoomRect = new Rect(0, 0, canvasWidth, canvasHeight);
canvas.addEventListener("mousemove", event => {
    let canvasRect = canvas.getBoundingClientRect();
    mouseX = event.clientX - canvasRect.x;
    mouseY = event.clientY - canvasRect.y;
    translatedMouseX = mouseX / scale - zoomRect.x / scale;
    translatedMouseY = mouseY / scale - zoomRect.y / scale;

    updateDebugText();
});


function drawCircle(x, y, r, color = 'green') {
    device.beginPath();
    device.arc(x, y, r, 0, 2 * Math.PI, false);
    device.fillStyle = color;
    device.fill();
}


function changeCurrentLevel(direction) {
    currentLevelIndex -= direction;
    if (currentLevelIndex >= levels.length) {
        currentLevelIndex = 0;
    } else if (currentLevelIndex < 0) {
        currentLevelIndex = levels.length - 1;
    }
    scale = 1;
    zoomRect.w = canvasWidth;
    zoomRect.h = canvasHeight;
    zoomRect.x = 0;
    zoomRect.y = 0;
    if (bitmapCache[currentLevelIndex] != null) {
        device.drawImage(bitmapCache[currentLevelIndex], 0, 0, canvasWidth, canvasHeight);
    }
    updateDebugText();
}

function drawVertex(vertex, color = 'green') {
    let xRange = maps[currentLevelIndex].maxX - maps[currentLevelIndex].minX;
    let yRange = maps[currentLevelIndex].maxY - maps[currentLevelIndex].minY;

    let x = (vertex.x - maps[currentLevelIndex].minX) / xRange * zoomRect.w + zoomRect.x;
    let y = (vertex.y - maps[currentLevelIndex].minY) / yRange * zoomRect.h + zoomRect.y;
    if (x > 0 && x < canvasWidth && y > 0 && y < canvasHeight) {
        drawCircle(x, y, 5, color);
    }
}

function drawVertices() {
    vertices[currentLevelIndex].forEach(v => {
        drawVertex(v);
    });
}

function getVertexMouseCollision() {
    let xRange = maps[currentLevelIndex].maxX - maps[currentLevelIndex].minX;
    let yRange = maps[currentLevelIndex].maxY - maps[currentLevelIndex].minY;
    var retValue = null;
    vertices[currentLevelIndex].forEach(v => {
        let x = (v.x - maps[currentLevelIndex].minX) / xRange * zoomRect.w + zoomRect.x;
        let y = (v.y - maps[currentLevelIndex].minY) / yRange * zoomRect.h + zoomRect.y;
        if (x > mouseX - 5 && x < mouseX + 5 && y > mouseY - 5 && y < mouseY + 5) {
            retValue = v;
            return v;
        }
    });
    return retValue;
}

function getActiveEdgesForVertex(vertex) {
    fetch(backendBaseURL + "Edges?vertexID=" + vertex.id)
        .then(result => result.json())
        .then(edges => {
            activeEdges = edges;
            draw();
        });
}


function loadVertices() {
    for (let i = 0; i < levels.length; i++) {
        let from = 0;
        let count = 0;
        console.log(backendBaseURL + "Vertecies/Start?level=" + maps[i].level)

        fetch(backendBaseURL + "Vertecies/Start?level=" + maps[i].level)
            .then(result => result.json())
            .then(data => {
                from = data

                fetch(backendBaseURL + "Vertecies/Count?level=" + maps[i].level)
                    .then(result => result.json())
                    .then(data => {
                        count = data;
                        console.log(`From: ${from} Count: ${count}`);

                        fetch(backendBaseURL + `Vertecies?start=${from}&count=${count}`)
                            .then(response => response.json())
                            .then(data => {
                                vertices[i] = data;
                                console.log("Loaded Vertices for Level:" + i);
                            });
                    });

            });
    }

}

function GetBitmapFromMapLevel(index, level) {
    console.log("Fetching from: " + backendBaseURL + "Maps/Bitmap/" + level + " for index:" + index);
    fetch(backendBaseURL + "Maps/Bitmap/" + level)
        .then(data => data.blob())
        .then(imageBlob => {
            const imageObjectURL = URL.createObjectURL(imageBlob);
            const img = new Image;
            img.onload = function () {
                console.log("Loaded level:" + level + " at Index:" + index);
                if (level === 1) {
                    console.log("Drawing first level");
                    changeCurrentLevel(0);
                }
            };
            img.src = imageObjectURL;
            bitmapCache[index] = img;
        });
}

function updateDebugText() {
    if (activeVertex != null) {
        debugLabel.innerText = `Active Vertex:${activeVertex.id} \n`;
    } else {
        debugLabel.innerText = "";
    }
    debugLabel.innerText +=
        `Level:${levels[currentLevelIndex]}: ${JSON.stringify(maps[currentLevelIndex])} 
        Zoomfactor:${scale} 
        Mouse{${mouseX},${mouseY}} TranslatedMouse{${translatedMouseX},${translatedMouseY}}
        Zoomrect{${roundToPixel(zoomRect.x)},${roundToPixel(zoomRect.y)},${roundToPixel(zoomRect.w)},${roundToPixel(zoomRect.h)} }`;
}

function roundToPixel(number) {
    return number.toFixed(1);
}