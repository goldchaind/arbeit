using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using InfoRouteBackend.InfoGraph;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;

namespace EffizienzTest
{
    [TestClass]
    public class UnitTest1
    {
        InfoGraph infoGraph;

        private struct RoutePoint
        {
            public int Start;
            public int End;
            public RoutePoint(int start, int end)
            {
                Start = start;
                End = end;
            }
        }

        RoutePoint[] routes =
        {
            new RoutePoint(32153,   32245),
            new RoutePoint(32175,   31732),
            new RoutePoint(32101,   51959),
            new RoutePoint(32166,   35607),
            new RoutePoint(31624,   26709)
        };


        public void Init()
        {
            var serviceProvider = new ServiceCollection()
            .AddLogging()
            .BuildServiceProvider();

            var factory = serviceProvider.GetService<ILoggerFactory>();


            var logger = factory.CreateLogger<InfoGraph>();

            infoGraph = new InfoGraph(logger);
        }

        [TestMethod]
        public void TestMethod1()
        {
            return;
            Init();
            Stopwatch[] times = new Stopwatch[5];
            int[] VertexCounts = new int[5];
            TimeSpan alltimesCombined = new TimeSpan();
            
            for (int routeID = 0; routeID < 5; routeID++)
            {
                times[routeID] = new Stopwatch();
                for (int i = 0; i < 5; i++)
                {
                    times[routeID].Start();
                    A_Star<InfoVertex> path = new A_Star<InfoVertex>(infoGraph, new InfoEuklidHeuristic(), infoGraph.GetVertexById(routes[routeID].Start), infoGraph.GetVertexById(routes[routeID].End));
                    path.CalculatePath();
                    VertexCounts[routeID] = path.Path.Length;
                    times[routeID].Stop();
                }
                Console.WriteLine("Route {0} took an average of {1} to calc for {2} Vertexes which is a speed of {3} Vertex/Second", routeID, times[routeID].Elapsed / 5,VertexCounts[routeID], VertexCounts[routeID] / (times[routeID].Elapsed.TotalSeconds / 5));
                alltimesCombined += times[routeID].Elapsed;
            }
            double speed0 = VertexCounts[0] / (times[0].Elapsed.TotalSeconds / 5);
            double speed4 = VertexCounts[4] / (times[4].Elapsed.TotalSeconds / 5);

            Console.WriteLine("Average Route Calculation Time is {0}", alltimesCombined / 25);
            Console.WriteLine("Short Route Speed to Long Route Speed Ratio is {0}", speed4 / speed0);
        }

        private class TestGraph : IGraph<Vertex>
        {
            public int VerticesCount => throw new NotImplementedException();

            public int EdgesCount => throw new NotImplementedException();

            public List<Vertex> Vertices  {get;set;}

            public EdgeList<Vertex> Edges  { get; set; }

            public RouteCache<Vertex> RouteCache => null;

            public TestGraph()
            {
                Vertices = new List<Vertex>();
                Edges = new EdgeList<Vertex>();
            }

            public List<Edge<Vertex>> GetEdgesForVertex(Vertex v)
            {
                return Edges[v.ID];
            }

            public Vertex GetVertexById(int id)
            {
                throw new NotImplementedException();
            }

        }

        private class TestHeuristic : Heurisitc<Vertex>
        {
            public double CalculateHeuristic(Vertex vertex1, Vertex vertex2)
            {
                return 0;
            }
        }

        private TestGraph GetGraph()
        {
            TestGraph testGraph = new TestGraph();

            for (int i = 0; i < 8; i++)
            {
                testGraph.Vertices.Add(new Vertex(i));
            }
            List<Vertex> vertices = testGraph.Vertices;

            testGraph.Edges.AddEdge(new Edge<Vertex>(1, vertices[0], vertices[1], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(2, vertices[1], vertices[6], 17));
            testGraph.Edges.AddEdge(new Edge<Vertex>(3, vertices[1], vertices[2], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(4, vertices[2], vertices[3], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(5, vertices[3], vertices[4], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(6, vertices[3], vertices[6], 3.4));
            testGraph.Edges.AddEdge(new Edge<Vertex>(7, vertices[4], vertices[5], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(8, vertices[6], vertices[7], 1.7));
            testGraph.Edges.AddEdge(new Edge<Vertex>(9, vertices[7], vertices[5], 1.7));

            return testGraph;
        }


        [TestMethod]
        public void TestCheckAnnotation()
        {
            TestGraph testGraph = GetGraph();
            AnnotationManager annotationManager = new AnnotationManager();
            CheckAnnotation ticket = annotationManager.AddAvailableAnnotation<CheckAnnotation>(new CheckAnnotation("Ticket"));
            A_Star<Vertex> testPathFinderActive = new A_Star<Vertex>(testGraph, new TestHeuristic(), testGraph.Vertices[0], testGraph.Vertices[5], annotationManager);
            A_Star<Vertex> testPathFinderInActive = new A_Star<Vertex>(testGraph, new TestHeuristic(), testGraph.Vertices[0], testGraph.Vertices[5], annotationManager);

            testGraph.Vertices[2].Annotation = ticket;
            annotationManager.CalculateAnnotationHirarchy();

            testPathFinderActive.CalculatePath();            
            Assert.AreEqual("012345", PathToString(testPathFinderActive.Path));

            ticket.Active = false;
            testPathFinderInActive.CalculatePath();
            Assert.AreEqual("01675", PathToString(testPathFinderInActive.Path));
        }

        [TestMethod]
        public void TestTimeAnnotation()
        {
            TestGraph testGraph = GetGraph();
            AnnotationManager annotationManager = new AnnotationManager();
            A_Star<Vertex> testPathFinderInactive = new A_Star<Vertex>(testGraph, new TestHeuristic(), testGraph.Vertices[0], testGraph.Vertices[5], annotationManager);
            A_Star<Vertex> testPathFinderActive = new A_Star<Vertex>(testGraph, new TestHeuristic(), testGraph.Vertices[0], testGraph.Vertices[5], annotationManager);
            
            TimeAnnotation timeAnnotation = annotationManager.AddAvailableAnnotation<TimeAnnotation>(new TimeAnnotation("Ticket"));

            timeAnnotation.ActiveTimes.Add(new TimeSpan(17, 0, 0), new TimeSpan(1, 0, 0));
            testGraph.Vertices[4].Annotation = timeAnnotation;
            annotationManager.CalculateAnnotationHirarchy();

            testPathFinderActive.calculationStartTime = new TimeSpan(17, 59, 56);
            testPathFinderActive.CalculatePath();
            Assert.AreEqual("012345", PathToString(testPathFinderActive.Path));
            
            testPathFinderInactive.calculationStartTime = new TimeSpan(18, 59, 50);
            testPathFinderInactive.CalculatePath();
            Assert.AreEqual("0123675", PathToString(testPathFinderInactive.Path));

        }


        private String PathToString(Vertex[] vertices)
        {
            string ret = "";
            foreach(Vertex v in vertices)
            {
                ret += v.ID;
            }
            return ret;
        }
       
    }
}