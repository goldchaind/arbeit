Freitag:
Editorwindow machen:
Graphen auf die Levelbilder abbilden
Dynamisches nachhladen der Graphenteile, nicht den kompletten auf einmal
Wegfindung zwischen zwei Punkten implementieren


Bis Sonntag:

Gliederungspunkte 1-6 schreiben:
Samstag:

1: 	ca 1-2 Stunden. Einfach frei runterschreiben

2: 	Nach Literaturrecherche gehen, in der Bib 2-3 B�cher dazu suchen und 4-5 Webquellen finden. 
	Roten Faden �berlegen, damit das Kapitel gut strukturiert wirkt
	Code der Algorithmen in VS Studio implementieren
	ca. 5-6 Stunden

3:	1-2 B�cher �ber Software Qualit�t zitieren und ein paar Webquellen.
	Implementierte Algorithmen durchlaufen lassen f�r einen Vergleich
	Testsoftware generieren, welche zu jedem Test einen Report schreibt, um keine Excel Tabelle f�hren zu m�ssen
	2-3 Stunden

8-11 Seiten am Samstag fertig haben

Sonntag:

4:	Freihand runterschreiben, kleine Referenzen an Webquellen, wegen der Frameworks usw.
	2 Stunden

5:	In VS implementieren und Test laufen lassen
	2-3 Stunden

6:	Eigene Ideen, viel Literaturrecherche und Webquellen zitieren
	Effizienztests nach den Schritten durchf�hren und in eine Exceltabelle speichern
	Erweiterung des Testprogramms um eine Speichernde Komponente
	5-6 Stunden

Programmieren:
L�sung �berlegen um Wege die �ber mehrere Ebenen gehen besser darzustellen
Verf�gbarkeitsstatus von Knoten variabel machen
Vorrausschauende Komponente implementieren mi Simzeit und annotiertem Graphen